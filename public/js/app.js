var app = {};


(function ($) {

    app.queue = function () {

        this.init = function () {

            this.initLinks = ['/crawl/clear', '/crawl/lines', '/crawl/stops'];
            this.linksCount = this.initLinks.length;
            this.currentCount = 0;
            this.linesLinks = [];
            this.call();


        };


        this.call = function () {
            if (this.currentCount < this.linksCount) {
                console.log(this.initLinks[this.currentCount]);
                $.get(this.initLinks[this.currentCount], $.proxy(this.sendRequest, this));
            } else {
                this.getLines();
            }
        };

        this.sendRequest = function (a) {

            console.log(a);
            this.currentCount++;
            this.call();
        };

        this.getLines = function () {
            $.get('/crawl/list-lines', $.proxy(function (a) {
                $.each(a, $.proxy(function (a, e) {
                    this.linesLinks.push('/crawl/departures/' + e.number);

                }, this));
                this.linksCount = this.linesLinks.length;
                this.currentCount = 0;
                this.callLine();
            }, this));

        };

        this.callLine = function () {
            if (this.currentCount < this.linksCount) {
                console.log(this.linesLinks[this.currentCount]);
                $.get(this.linesLinks[this.currentCount], $.proxy(this.sendRequestToLine, this));
            } else {
                console.log('done!');
            }
        };

        this.sendRequestToLine = function (a) {

            console.log(a);
            this.currentCount++;
            this.callLine();
        };


        this.init();

    };


    app.queue();

})(jQuery);