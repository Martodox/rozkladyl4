angular.module('timeTable', [
    'ngSanitize',
    'ngCookies',
    'ui.router',
    'uiGmapgoogle-maps'
]).config(function ($stateProvider, $urlRouterProvider) {

    'use strict';

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'views/selectStop.html',
            controller: 'HomeCtrl'
        })
        .state('stop', {
            url: "/:slug",
            templateUrl: 'views/selectStop.html',
            controller: 'HomeCtrl'
        })
        .state('map', {
            url: "/map/markers",
            templateUrl: 'views/map.html',
            controller: 'MapCtrl'
        });
    $urlRouterProvider.otherwise("/");
});