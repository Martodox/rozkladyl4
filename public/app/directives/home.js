/**
 * Directive: Inbox <inbox></inbox>
 */
angular.module('timeTable')
    .directive('stopsSelect', function StopsSelectDrctv(HomeFactory) {
        'use strict';

        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: "views/directives/home.html",
            controllerAs: 'home',

            controller: function (HomeFactory) {
                this.stops = [];

                this.goToStop = function (id) {

                    HomeFactory.goToStop(id);
                };

                this.getFavoriteStop = function () {
                    HomeFactory.getFavoriteStop();

                }

            },


            link: function (scope, element, attrs, ctrl) {
                $.material.init();
                $('body').find('.perfect-scroll').perfectScrollbar({
                    suppressScrollX: true
                });

            }
        }
    });