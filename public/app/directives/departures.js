/**
 * Directive: Inbox <inbox></inbox>
 */
angular.module('timeTable')
    .directive('departures', function DeparturesDrctv() {
        'use strict';

        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: "views/directives/departures.html",
            controllerAs: 'home',
            link: function (scope, element, attrs, ctrl) {
                $.material.init();
                $('body').find('.perfect-scroll').perfectScrollbar({
                    suppressScrollX: true
                });

            }
        }
    });