/**
 * Directive: Inbox <clock></clock>
 */
angular.module('timeTable')
    .directive('clock', function ClockDrctv($timeout) {
        'use strict';


        return {
            restrict: 'E',
            transclude: true,
            link: function (scope, element, attrs) {
                function addZero(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }

                var tick = function () {
                    var exact = new Date();
                    var text = addZero(exact.getHours()) + ':' + addZero(exact.getMinutes());
                    element.html(text);
                    $timeout(tick, 5000);
                }

                $timeout(tick, 1000);
            }


        }
    });