/**
 * Factory: InboxFactory
 */
angular.module('timeTable')
    .factory('HomeFactory', function HomeFactory($q, $rootScope, $state, $http, $cookies, $timeout) {
        'use strict';
        var exports = {};


        exports.getStops = function () {

            return $http.get('/api/v1/stops')
                .success(function (data) {
                    $rootScope.stops = data;
                    exports.getFavoriteStop();
                });

        };

        exports.listDepartures = function (params, noFilters) {

            if (typeof params != 'object') {
                if (parseInt($cookies.favStop) > 0) {
                    $rootScope.favStop = $cookies.favStop;
                }

                $http.get('/api/v1/departures/' + params)
                    .success(function (data) {

                        if (typeof data.lines == 'undefined') {
                            return exports.getFavoriteStop(true);
                        }

                        $rootScope.departures = data;
                        if (noFilters != true) {
                            $rootScope.filters.lineNumber = '';
                        }
                        $timeout(function () {
                            exports.listDepartures(params, true);
                        }, 60000);
                    });

            }

        };

        exports.goToStop = function (id) {
            exports.listDepartures(id);
        };

        exports.addAsFavorite = function (id) {

            if (id.length > 0) {
                if ($cookies.favStop != $rootScope.favStop || $rootScope.favStop != id) {
                    $cookies.favStop = id;
                    $rootScope.favStop = id;
                } else {
                    $cookies.favStop = "";
                    $rootScope.favStop = "";
                }

            }

        };

        exports.getFavoriteStop = function (force) {
            if (typeof $cookies.favStop != 'undefined') {
                if ($cookies.favStop.length > 0 && (!$rootScope.slug || force)) {

                    $state.go('stop', {slug: $rootScope.favStop});
                }
            }

        };

        exports.stopSearch = function (phrase) {

            if (phrase != "") {
                $http.get('/api/v1/stops/' + phrase)
                    .success(function (data) {
                        if (data.length > 0) {
                            $rootScope.stops = data;
                        } else {
                            $rootScope.stops = false;
                        }
                    });
            } else {
                $rootScope.stops = false;
            }


        };

        return exports;
    }).
    filter('linesFilter', function () {
        return function (lines, filterVal) {
            var linesCp = [];
            if (filterVal.lineNumber != "") {
                angular.forEach(lines, function (val, key) {
                    if (val.lineNumber === filterVal.lineNumber) {
                        linesCp.push(val);
                    }
                })
                return linesCp;
            } else {
                return lines;
            }


        };
    })
;


