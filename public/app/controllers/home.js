/**
 * Controller: HomeCtrl
 */
angular.module('timeTable')
    .controller('HomeCtrl',
    function HomeCtrl($scope, $rootScope, $stateParams, $cookies, $state, $http, HomeFactory) {
        'use strict';
        $rootScope.filters = {};
        $rootScope.favStop = null;
        $rootScope.slug = $stateParams.slug;
        $rootScope.HomeFactory = HomeFactory;
        $rootScope.favStop = $cookies.favStop;
        $scope.showMap = false;
        $scope.showSearch = false;
        if ($rootScope.slug) {
            HomeFactory.listDepartures($rootScope.slug);
        }
        else {
            HomeFactory.getFavoriteStop();
        }


        $scope.closeMap = function () {
            $scope.showMap = false;
        };

        $scope.closeSearch = function () {
            $scope.showSearch = false;
        };

        $scope.toggleMap = function () {

            $scope.closeSearch();
            $scope.showMap = !$scope.showMap;
            $scope.loadMap();
        };

        $scope.toggleSearch = function () {
            $scope.closeMap();
            $scope.showSearch = !$scope.showSearch;
        };

        $scope.loadMap = function () {
            angular.extend($scope, {
                map: {
                    center: {
                        longitude: 19.94266,
                        latitude: 50.05941
                    },
                    options: {
                        streetViewControl: false
                    },
                    zoom: 15,
                    events: {
                        tilesloaded: function (map) {
                            $scope.mapLoaded(map);
                        }
                    }

                }
            });


            $scope.mapLoaded = function (map) {
                $scope.$apply(function () {


                    var bounds = map.getBounds();
                    var pos = {
                        lefttop: {
                            x: bounds.na.j,
                            y: bounds.va.j
                        },
                        rightbottom: {
                            x: bounds.na.k,
                            y: bounds.va.k
                        }
                    }
                    $scope.loadMarkers(pos, map.getZoom());
                });
            };


            $scope.onMarkerClicked = function (marker) {
                $state.go('stop', {slug: marker.slug});

            };


            $scope.loadMarkers = function (data, zoom) {

                if (zoom >= 13) {
                    $http.post('/api/v1/get-markers', data)
                        .success(function (data) {

                            var dynamicMarkers = data;

                            _.each(dynamicMarkers, function (marker) {
                                marker.closeClick = function () {
                                    marker.showWindow = false;
                                    $scope.$apply();
                                };
                                marker.onClicked = function () {
                                    $scope.onMarkerClicked(marker);
                                };
                            });
                            $scope.map.dynamicMarkers = dynamicMarkers;

                        });
                } else {
                    $scope.map.dynamicMarkers = [];
                }
            };
        };

    });