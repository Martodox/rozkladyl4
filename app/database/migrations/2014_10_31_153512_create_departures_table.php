<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeparturesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stop_id');
            $table->integer('line_id');
            $table->integer('route_id');
            $table->integer('hour');
            $table->integer('minute');
            $table->string('hhmm', 5);
            $table->smallInteger('dayflag');
            $table->index('hour');
            $table->index('minute');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('departures');
    }

}
