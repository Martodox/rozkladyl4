<!doctype html>
<!--suppress HtmlUnknownTarget, JSUnresolvedLibraryURL -->
<html>
<head>
    <meta charset="utf-8">
    <title>Rozkłady</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link href="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.css" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="/style/style.less">
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.0.0/less.min.js"></script>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>
<body ng-app="timeTable">
<script src="/js/analitycs.js"></script>

<div ui-view class="page-wrap"></div>


<script src="http://maps.googleapis.com/maps/api/js?libraries=weather,visualization&sensor=false&language=en&v=3.13"></script>
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-cookies.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-sanitize.min.js"></script>
<script src="//dylanfprice.github.io/angular-gm/1.0.0/angular-gm.min.js"></script>
<script src="/js/angular-ui-router.min.js"></script>
<script src="/js/perfect-scrollbar.js"></script>
<script src="/js/ripples.min.js"></script>
<script src="/js/material.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js" type="text/javascript"></script>
<script src="/js/angular-google-maps.js"></script>
<script src="/js/dynamic-map-height.js"></script>
<script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>



<script src="/app/app.js"></script>

<script src="/app/controllers/home.js"></script>
<script src="/app/controllers/map.js"></script>
<script src="/app/directives/home.js"></script>
<script src="/app/services/homefactory.js"></script>

<script src="/app/directives/departures.js"></script>
<script src="/app/directives/minutesTill.js"></script>
<script src="/app/directives/clock.js"></script>

</body>

</html>