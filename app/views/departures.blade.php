
    <table>
    <thead>
    <tr>
    <td>Line</td>
    <td>Type</td>
    <td>FROM - TO</td>
    <td>Hour</td>
    <td>Minute</td>
    <td>Exact</td>

    </tr>
    </thead>
    <tbody>
    @foreach ($data as $single)
    <tr>
        <td>{{$single['line-number']}}</td>
        <td>
        @if ($single['bus'])
        BUS
        @else
        TRAM
        @endif
        </td>
        <td>{{$single['route-details']['start_name']}} - {{$single['route-details']['end_name']}}</td>
        <td>{{$single['hour']}}</td>
        <td>{{$single['minute']}}</td>
        <td>{{$single['exact-time']}}</td>
    </tr>
    @endforeach
     </tbody>
     </table>
