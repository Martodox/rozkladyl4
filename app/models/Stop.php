<?php

class Stop extends Eloquent
{
    public $timestamps = false;

    protected $fillable = array('name', 'slug', 'posx', 'posy');
}