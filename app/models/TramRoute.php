<?php


class TramRoute extends Eloquent
{

    protected $table = 'routes';
    public $timestamps = false;

    protected $fillable = array('start_id', 'end_id');

    public function departure()
    {
        return $this->belongsTo('Departures');
    }

    public function stop()
    {
        return $this->belongsToMany('Stop');
    }
}