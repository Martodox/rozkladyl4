<?php

class Departures extends Eloquent
{
    public $timestamps = false;

    protected $fillable = array('stop_id', 'line_id', 'route_id', 'hour', 'minute', 'dayflag');

    public function stop()
    {
        return $this->belongsTo('Stop');
    }

    public function line()
    {
        return $this->belongsTo('Line');
    }
    public function route()
    {
        return $this->belongsTo('TramRoute');
    }
}