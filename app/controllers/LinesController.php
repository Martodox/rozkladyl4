<?php

class LinesController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $statusCode = 200;
        $response = [];

        $lines = Line::all();

        foreach ($lines as $line) {
            $response[] = [
                'id' => $line->id,
                'number' => $line->number
            ];
        }

        return Response::json($response, $statusCode);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $line = Line::find($id);
        if (empty($line)) {
            $responseCode = 400;
        } else {
            $responseCode = 200;
        }
        return Response::json($line, $responseCode);

    }


}
