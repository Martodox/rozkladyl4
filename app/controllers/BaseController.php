<?php

class BaseController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        header('Content-Type: text/html; charset=utf-8');
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

}
