<?php

class DeparturesController extends \BaseController
{


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * * @param  int $stop
     * @return Response
     */
    public function show($id, $number = 50, $timestamp = null)
    {
        $statusCode = 200;
        $response = [];
        date_default_timezone_set('CET');
        $timestamp = ($timestamp == null ? time() : $timestamp);
        $currentStop = -1;
        if (is_numeric($id)) {
            $currentStop = $id;
            $stopName = Stop::find($id);
        } else {
            $stopID = Stop::where('slug', '=', $id)->limit(1)->get();

            foreach ($stopID as $getID) {
                $currentStop = $getID->id;
                $stopName = $getID;
                break;
            }

        }

        if ($currentStop == -1) {
            return;
        }
        $currentTime = date("Y-m-d H:i", $timestamp);
        $tomorrow = date("Y-m-d H:i", $timestamp + 86400);
        if ($number > 50) {
            $number = 50;
        }
        $closests = DB::select("SELECT *, str_to_date(CONCAT(d.`date`, ' ', dp.`hhmm`), '%Y-%m-%d %H:%i') as exactdate
FROM ro_dates as d LEFT JOIN ro_departures as dp ON 1=1
WHERE dp.stop_id = ?
AND str_to_date(CONCAT(d.`date`, ' ', dp.`hour`,':',dp.`minute`), '%Y-%m-%d %H:%i') > ?
AND
 CASE
    WHEN DAYOFWEEK(d.`date`) = 1 THEN dp.dayflag = 2
    WHEN DAYOFWEEK(d.`date`) = 7 THEN dp.dayflag = 1
    ELSE dp.dayflag = 0
END
AND d.`date` < ?
ORDER BY d.date, dp.hour, dp.minute  LIMIT ?", array($currentStop, $currentTime, $tomorrow, $number));


        $routes = [];
        $lines = [];
        foreach ($closests as $time) {
            $response[] = [
                'id' => $time->id,
                'line_id' => $time->line_id,
                'lineNumber' => null,
                'hour' => $time->hour,
                'minute' => $time->minute,
                'route' => $time->route_id,
                'routeDetails' => null,
                'exactTime' => strtotime($time->exactdate),
                'bus' => null
            ];
            $lines[] = $time->line_id;
            $routes[] = $time->route_id;

        }


        $lines = array_unique($lines);
        $lines = Line::findMany($lines);
        $responseLines = [];
        foreach ($lines as $singleLine) {
            foreach ($response as &$oneResponse) {
                if ($oneResponse['line_id'] == $singleLine->id) {
                    $bus = false;
                    if ((int)$singleLine->number > 100) {
                        $bus = true;
                    }
                    $oneResponse['lineNumber'] = $singleLine->number;
                    $oneResponse['bus'] = $bus;
                    if (!in_array($singleLine->number, $responseLines)) {
                        $responseLines[] = $singleLine->number;
                    }

                }
            }
        }

        $routes = array_unique($routes);
        $routes = TramRoute::findMany($routes);
        $tramRoutes = [];
        $stopID = [];
        foreach ($routes as $route) {
            $tramRoutes[] = array(
                'id' => $route->id,
                'startId' => $route->start_id,
                'endId' => $route->end_id,
                'startName' => '',
                'endName' => ''
            );
            $stopID[] = $route->start_id;
            $stopID[] = $route->end_id;
        }

        $stopID = array_unique($stopID);
        $stopNames = Stop::findMany($stopID);

        foreach ($stopNames as $singleStop) {
            $id = $singleStop->id;

            foreach ($tramRoutes as &$route) {
                if ($route['startId'] == $id) {
                    $route['startName'] = $singleStop->name;
                }
                if ($route['endId'] == $id) {
                    $route['endName'] = $singleStop->name;
                }

            }
        }

        foreach ($response as &$singleResponse) {
            $id = $singleResponse['route'];
            unset($route);
            foreach ($tramRoutes as $route) {
                if ($route['id'] == $id) {
                    $singleResponse['routeDetails'] = $route;
                }
            }
        }


        //return View::make('departures')->with(array('data' => $response));

        return Response::json(array('stop' => $stopName, 'lines' => $responseLines, 'deps' => $response), $statusCode);
    }


}
