<?php


class StopsController extends \BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lines = Stop::all();
        return $this->parseResponse($lines);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * * @param  int $stop
     * @return Response
     */
    public function show($text)
    {
        $lines = Stop::where('name', 'LIKE', '%' . $text . '%')->limit(50)->get();
        return $this->parseResponse($lines);
    }

    public function  map()
    {


        $input = Input::all();


        $lines = Stop::
        where('posx', '>', $input['rightbottom']['y'])->
        where('posx', '<', $input['lefttop']['y'])->
        where('posy', '<', $input['rightbottom']['x'])->
        where('posy', '>', $input['lefttop']['x'])->
        get();


        return $this->parseResponse($lines);
    }

    public function updateLocation()
    {
        $input = Input::all();

        if (!empty($input)) {
            $idGet = Stop::where('name', '=', $input['name'])->limit(1)->get();
            foreach ($idGet as $id) {
                $toSave = Stop::find($id->id);
                $toSave->posx = $input['posx'];
                $toSave->posy = $input['posy'];
                $toSave->save();
            }
            print_r($input);
        }


        return View::make('simpleform');
    }

    private function parseResponse($lines)
    {
        $statusCode = 200;
        $response = [];
        foreach ($lines as $line) {
            $response[] = [
                'id' => $line->id,
                'name' => $line->name,
                'slug' => $line->slug,
                'latitude' => $line->posx,
                'longitude' => $line->posy,
                'options' => array(
                    'labelContent' => $line->name,
                    'labelClass' => 'map-marker-label'
                )

            ];
        }

        return Response::json($response, $statusCode);
    }


    public function strings()
    {
        $lines = Stop::all();
        $string = "";
        foreach ($lines as $line) {

            $string = $string . ",'" . $line->name . "'";


        }
        echo substr($string, 1);
    }
}